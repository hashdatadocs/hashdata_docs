#Cutomize Sphinx

##Installation
```
git clone /url/to/repo
cd /path/to/project
sudo npm install
```
---

##Use
- ###Local Debug
```
# 编译sphinx生成html页面
make html
# 之后只需要修改source/hashdatastatic/css/custom.css
# 为了不用每次更新后都需要执行make html指令
# 使用gulp来实时更新build/html/_static/css/custom.css
# 执行gulp,会监视/path/to/project/source/hashdatastatic/css/custom.css的变化
# 并实时更新到build/html/_static/css/custom.css中
gulp
```
- ###Deploy
使用readthedocs来部署，目前已在https://readthedocs.org/projects/hashdata-docs/builds 部署好.账号密码如下：<br/>
```
account: shshang
password: hashdata
```
<br/>只需要git push origin master提交到bitbucket即可。已配置好webhook去通知readthedocs执行make html指令实时更新<br/>
```
git push origin master
```

---

##Customize
需要修改有两部分<br/>

- css<br/>
对比当前页面和设计线框图的差别，修改source/hashdatastatic/css/custom.css. 如果当前页面内容不够多样化，可以自行修改source/index.rst来增加内容

- html<br/>
可能有部分页面的布局和当前主题不一致(当前主题已经是能找到的最接近设计稿的主题了)，需要修改source/sphinx_rtd_theme里的html页面(主要是layout.html)来修改布局，这里的html采用了jinjia模板，和django一致
