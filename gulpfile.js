var gulp = require('gulp');
var uglify = require('gulp-uglify');
var cleanCSS = require('gulp-clean-css');

gulp.task('cleancss', function () {
    gulp.src('source/hashdatastatic/css/custom.css')
        .pipe(gulp.dest('build/html/_static/css/'));
});

gulp.task('watch', function () {
    gulp.watch('source/hashdatastatic/css/custom.css', ['cleancss']);
});

gulp.task('default', ['build'])
gulp.task('build', ['cleancss', 'watch']);